
ftp-client
-
主要用於上傳次特店檔案到中信 FTP

build docker image
-
~~~bash
# 取名為 ftp-client
docker build -t ftp-client .
~~~

本機測試
-
設定網路
~~~bash
# create docker network for testing
docker network create ttnet
~~~

設定 FTP Server
~~~bash
# 使用 pure-ftpd 作為 FTP server
docker pull stilliard/pure-ftpd:hardened

# 啟動 FTP server，取名為 ftpd_server，並加入 ttnet 網路
docker run -d --net ttnet --name ftpd_server -p 21:21 -p 30000-30009:30000-30009 -e "PUBLICHOST=localhost" stilliard/pure-ftpd:hardened

# 連線到 ftpd_server 中執行 bash
docker exec -it ftpd_server /bin/bash

# 新增 FTP 使用者：hap，密碼：hap
pure-pw useradd hap -f /etc/pure-ftpd/passwd/pureftpd.passwd -m -u ftpuser -d /home/ftpusers/hap
~~~

使用 ftp-client 上傳檔案
~~~bash
# 本機測試時才需加入 ttnet 網路
# mount 檔案位於主機的目錄到 docker 的 /tmp 目錄
# 指定 HOST, USER, PASS, FILE 變數
docker run --net ttnet -v <path_to_host_file>:/tmp -e "HOST=ftpd_server" -e "USER=hap" -e "PASS=hap" -e "FILE=ctbcmsub.zip" ftp-client
~~~

中信 FTP 連線資訊（須使用支付樂主機才連得到）
-
host: 203.66.193.20  
user: happy_acq@tradevanftp@203.66.193.20  
password: ja29ix6k@traftp01  
