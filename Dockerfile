FROM debian:jessie

# installs ftp
RUN apt-get update && apt-get install -y ftp

RUN mkdir -p /tmp
WORKDIR /tmp

# setup ftpput.sh file
COPY ftpput.sh /ftpput.sh
RUN chmod u+x /ftpput.sh

# startup
CMD /ftpput.sh
